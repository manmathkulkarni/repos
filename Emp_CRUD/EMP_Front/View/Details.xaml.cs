﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using EMP_Data.Handler;
using EMP_Data.Models;
using EMP_Front.Controls;

namespace EMP_Front
{
    /// <summary>
    /// Interaction logic for Details.xaml
    /// </summary>
    public partial class Details : Window
    {
        #region Variables
        private CustomTable customTable;
        private FlowDocumentScrollViewer _tf1;

        int columns = 3;
        #endregion

        public Employee employee { get; set; }
        public Details()
        {
            InitializeComponent();
            CreateAndShowMainWindow();
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            if (employee != null) ShowEmployeeDetails();
        }


        #region designing the detail page
        private void CreateAndShowMainWindow()
        {
            // Create the parent viewer...
            _tf1 = new FlowDocumentScrollViewer { Document = new FlowDocument() };

            // Create the Table
            customTable = new CustomTable(columns);
            _tf1.Document.Blocks.Add(customTable._table1);

            CustomButton btnBack = new CustomButton("Back to list", ButtonType.Default);
            btnBack._button.Click += backToList;
            customTable.AddTitle("Employee Details", btnBack);


            this.Title = "Employee Management System";
            this.Content = _tf1;
        }
        private void ShowEmployeeDetails()
        {
            TableRowGroup infoGroup = new TableRowGroup();
            infoGroup.FontSize = 16;

            TableRow infoRow = new TableRow();
            TableCell dataCell = new TableCell();

            infoRow = AddData("Id: ", employee.id.ToString(), infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddData("Name: ", employee.name, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddData("Email: ", employee.email, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddData("Gender: ", employee.gender, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddData("Status: ", employee.status, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddData("Created At: ", employee.created_at, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddData("Updated At: ", employee.updated_at, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);


            // edit button (footer)
            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddFooter(infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            customTable._table1.RowGroups.Add(infoGroup);
        }

        private TableRow AddData(string column, string data, TableRow infoRow, TableCell dataCell)
        {
            infoRow.Cells.Add(new TableCell(new Paragraph(new Run(column))));

            dataCell.FontWeight = FontWeights.Bold;
            dataCell.Blocks.Add(new Paragraph(new Run(data)));
            infoRow.Cells.Add(dataCell);

            infoRow.Cells[1].ColumnSpan = 2;
            return infoRow;
        }
        private TableRow AddFooter(TableRow infoRow, TableCell dataCell)
        {
            StackPanel btnPanel = new StackPanel();
            btnPanel.HorizontalAlignment = HorizontalAlignment.Left;
            btnPanel.Orientation = Orientation.Vertical;

            CustomButton btnEdit = new CustomButton("Edit");
            btnEdit._button.Click += editEmployee;
            btnEdit._button.Tag = employee;
            btnPanel.Children.Add(btnEdit._button);

            dataCell.Blocks.Add(new BlockUIContainer(btnPanel));
            infoRow.Cells.Add(new TableCell());
            infoRow.Cells.Add(dataCell);
            infoRow.Cells[1].ColumnSpan = 2;

            return infoRow;
        }

        #endregion


        private void editEmployee(object sender, RoutedEventArgs e)
        {
            Add edit = new Add();
            edit.isEditMode = true;
            edit.employee = employee;
            edit.Show();
            this.Close();
        }

        public void backToList(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}
