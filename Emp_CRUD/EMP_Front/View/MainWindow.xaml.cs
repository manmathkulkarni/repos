﻿using Emp_CRUD.Access;
using EMP_Data.Handler;
using EMP_Data.Models;
using EMP_Front.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EMP_Front
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Variables

        private CustomTable customTable;
        private FlowDocumentScrollViewer _tf1;

        int pageNum = 1, total = 0, pageLimit = 0, maxPage = 0;
        int columns = 6;

        bool isSearchOn = false;

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            CreateAndShowMainWindow();
        }
        private async void OnLoad(object sender, RoutedEventArgs e)
        {
            await GetAllEmployees();
        }
        private void CreateAndShowMainWindow()
        {
            // Create the parent viewer...
            _tf1 = new FlowDocumentScrollViewer { Document = new FlowDocument() };

            // Create the Table
            customTable = new CustomTable(columns);
            _tf1.Document.Blocks.Add(customTable._table1);

            CustomButton btnAddEmp = new CustomButton("Add Employee", ButtonType.Default);
            btnAddEmp._button.Click += addEmployee;
            customTable.AddTitle("List of Employees", btnAddEmp);

            string[] headers = { "Id", "Name", "Email", "Gender", "Status" };
            customTable.AddHeader(headers, true);
            customTable.searchBtn._button.Click += searchEmployees;

            customTable.txtSearchBox.Text = "Search Employee..";
            customTable.txtSearchBox.GotFocus += HidePlaceHolder;
            customTable.txtSearchBox.LostFocus += ShowPlaceHolder;

            this.Title = "Employee Management System";
            this.Content = _tf1;
            this.Show();
        }

        private async Task GetAllEmployees()
        {
            customTable.ShowLoader();
            ServerResponse response = new ServerResponse();
            if (!isSearchOn) response = await EmployeeHandler.Get(null, pageNum);
            else
            {
                Employee emp = new Employee() { name = customTable.txtSearchBox.Text.Trim().ToLower() };
                response = await EmployeeHandler.Get(emp, pageNum);
            }

            if (response != null && response.success)
            {
                customTable.CreateRowGroupForData();
                if (response.data != null && response.data.Count > 0)
                {
                    foreach (var employee in response.data)
                    {
                        string[] data = { employee.id.ToString(), employee.name, employee.email, employee.gender, employee.status };

                        List<CustomButton> btns = new List<CustomButton>();

                        CustomButton viewBtn = new CustomButton("View", ButtonType.Link);
                        viewBtn._button.Click += viewEmployee;
                        viewBtn._button.Tag = employee;
                        btns.Add(viewBtn);

                        CustomButton deleteBtn = new CustomButton("Remove", ButtonType.Link);
                        deleteBtn._button.Click += removeEmployee;
                        deleteBtn._button.Tag = employee;
                        btns.Add(deleteBtn);

                        customTable.AddRow(data, btns);
                    }

                    if (response.meta != null && response.meta.pagination != null)
                    {
                        pageNum = response.meta.pagination.page;
                        total = response.meta.pagination.total;
                        pageLimit = response.meta.pagination.limit;
                        maxPage = response.meta.pagination.pages;
                    }

                    CustomButton btnPrev = new CustomButton("Previous");
                    btnPrev._button.Click += GotoPrevPage;

                    CustomButton btnNext = new CustomButton("Next");
                    btnNext._button.Click += GotoNextPage;

                    CustomButton btnFirst = new CustomButton("First");
                    btnFirst._button.Click += GotoFirstPage;

                    CustomButton btnLast = new CustomButton("Last");
                    btnLast._button.Click += GotoLastPage;

                    List<CustomButton> footerButtons = new List<CustomButton>();
                    footerButtons.Add(btnFirst); footerButtons.Add(btnPrev); footerButtons.Add(btnNext); footerButtons.Add(btnLast);
                    customTable.AddFooter(pageNum, maxPage, pageLimit, total, footerButtons);
                }
                else
                {
                    customTable.NoRecordFound();
                }

            }
            customTable.HideLoader();
        }

        public async void searchEmployees(object sender, RoutedEventArgs e)
        {
            pageNum = 1; customTable.RemoveAllData();
            if (customTable.txtSearchBox != null && customTable.txtSearchBox.Text != "Search Employee..")
            {
                isSearchOn = true;
                await GetAllEmployees();
            }
            else
            {
                isSearchOn = false;
                await GetAllEmployees();
            }
        }


        #region click event to got to Next, Previous, First, Last
        public async void GotoNextPage(object sender, RoutedEventArgs e)
        {
            if (maxPage <= pageNum) return;

            pageNum = pageNum + 1;
            customTable.RemoveAllData();

            await GetAllEmployees();
        }
        public async void GotoPrevPage(object sender, RoutedEventArgs e)
        {
            if (pageNum - 1 < 1) return;

            pageNum = pageNum - 1;
            customTable.RemoveAllData();

            await GetAllEmployees();
        }
        public async void GotoFirstPage(object sender, RoutedEventArgs e)
        {
            if (pageNum == 1) return;

            pageNum = 1;
            customTable.RemoveAllData();
            await GetAllEmployees();
        }
        public async void GotoLastPage(object sender, RoutedEventArgs e)
        {
            if (pageNum == maxPage) return;

            pageNum = maxPage;
            customTable.RemoveAllData();
            await GetAllEmployees();
        }
        #endregion

        #region All button events related to Add/View/Remove employee
        public async void removeEmployee(object sender, RoutedEventArgs e)
        {
            var employee = (Employee)((Button)sender).Tag;
            if (employee != null)
            {
                ServerResponse response = await EmployeeHandler.Delete(employee);
                if (response != null && response.success)
                {
                    customTable.ShowStatus(employee.name + "(" + employee.id + ") removed successfully.");
                    customTable.RemoveAllData();
                    await GetAllEmployees();
                }
            }
        }
        public void addEmployee(object sender, RoutedEventArgs e)
        {
            Add add = new Add();
            add.isEditMode = false;
            add.Show();
            this.Close();
        }
        public void viewEmployee(object sender, RoutedEventArgs e)
        {
            var employee = (Employee)((Button)sender).Tag;
            if (employee != null)
            {
                Details details = new Details();
                details.employee = employee;

                details.Show();
                this.Close();
            }
        }

        #endregion

        #region Show/Hide placeholder for Search box
        public void HidePlaceHolder(object sender, EventArgs e)
        {
            if (customTable.txtSearchBox.Text == "Search Employee..") customTable.txtSearchBox.Text = "";
        }
        public void ShowPlaceHolder(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(customTable.txtSearchBox.Text)) customTable.txtSearchBox.Text = "Search Employee..";
        }
        #endregion
    }
}
