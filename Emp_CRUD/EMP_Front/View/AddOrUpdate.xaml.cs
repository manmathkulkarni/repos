﻿using EMP_Data.Handler;
using EMP_Data.Models;
using EMP_Data.Utility;
using EMP_Front.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EMP_Front
{
    /// <summary>
    /// Interaction logic for Add.xaml
    /// </summary>
    public partial class Add : Window
    {

        #region Variables
        FlowDocumentScrollViewer _tf1;
        CustomTable customTable;
        int columns = 3;
        public Employee employee { get; set; }
        public bool isEditMode { get; set; }

        TextBox txtName; TextBox txtEmail;
        ComboBox ddGender; ComboBox ddStatus;
        #endregion
        public Add()
        {
            InitializeComponent();
            CreateAndShowMainWindow();
        }
        private void OnLoad(object sender, RoutedEventArgs e)
        {
            ShowAddForm();
        }



        #region OnLoad Designing the Employee Form
        private void CreateAndShowMainWindow()
        {
            // Create the parent viewer...
            _tf1 = new FlowDocumentScrollViewer { Document = new FlowDocument() };

            // Create the Table
            customTable = new CustomTable(columns);
            _tf1.Document.Blocks.Add(customTable._table1);

            CustomButton btnBack = new CustomButton("Back to list", ButtonType.Default);
            btnBack._button.Click += backToList;
            customTable.AddTitle("Employee Form", btnBack);

            this.Title = "Employee Management System";
            this.Content = _tf1;

            employee = new Employee();
        }
        private void ShowAddForm()
        {
            TableRowGroup infoGroup = new TableRowGroup();
            infoGroup.FontSize = 16;

            TableRow infoRow = new TableRow();
            TableCell dataCell = new TableCell();

            if (isEditMode)
            {
                infoRow = AddRow("Id: ", TextBoxType.id, infoRow, dataCell);
                infoGroup.Rows.Add(infoRow);
            }

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddRow("Name: ", TextBoxType.name, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddRow("Email: ", TextBoxType.email, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddRow("Gender: ", TextBoxType.gender, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);

            infoRow = new TableRow(); dataCell = new TableCell();
            infoRow = AddRow("Status: ", TextBoxType.status, infoRow, dataCell);
            infoGroup.Rows.Add(infoRow);


            // submit button (footer)
            infoRow = new TableRow();
            infoRow = AddFooter(infoRow);
            infoGroup.Rows.Add(infoRow);

            customTable._table1.RowGroups.Add(infoGroup);
        }
        private TableRow AddRow(string column, TextBoxType txtType, TableRow infoRow, TableCell dataCell)
        {
            infoRow.Cells.Add(new TableCell(new Paragraph(new Run(column))));

            StackPanel panel = new StackPanel();

            switch (txtType)
            {
                case TextBoxType.id:
                    Paragraph para_id = new Paragraph(new Run(employee.id.ToString()));
                    para_id.FontWeight = FontWeights.Bold;
                    dataCell.Blocks.Add(para_id);
                    break;

                case TextBoxType.email:
                    txtEmail = new TextBox();
                    txtEmail.TextChanged += EmpEmailChanged;
                    if (isEditMode) txtEmail.Text = employee.email;
                    panel.Children.Add(txtEmail);
                    break;
                case TextBoxType.name:
                    txtName = new TextBox();
                    txtName.TextChanged += EmpNameChanged;
                    if (isEditMode) txtName.Text = employee.name;
                    panel.Children.Add(txtName);
                    break;
                case TextBoxType.gender:
                    ddGender = new ComboBox();
                    ddGender.Items.Add(Genders.Male.ToString()); ddGender.Items.Add(Genders.Female.ToString());
                    ddGender.SelectionChanged += EmpGenderChanged;

                    if (isEditMode)
                    {
                        if (Genders.Male.ToString() == employee.gender) ddGender.SelectedIndex = (int)Genders.Male;
                        else if (Genders.Female.ToString() == employee.gender) ddGender.SelectedIndex = (int)Genders.Female;
                    }
                    else ddGender.SelectedIndex = 0;

                    panel.Children.Add(ddGender);
                    break;
                case TextBoxType.status:
                    ddStatus = new ComboBox();
                    ddStatus.Items.Add(Status.Active.ToString()); ddStatus.Items.Add(Status.Inactive.ToString());
                    ddStatus.SelectionChanged += EmpStatusChanged;

                    if (isEditMode)
                    {
                        if (Status.Active.ToString() == employee.status) ddStatus.SelectedIndex = (int)Status.Active;
                        else if (Status.Inactive.ToString() == employee.status) ddStatus.SelectedIndex = (int)Status.Inactive;
                    }
                    else ddStatus.SelectedIndex = 0;

                    panel.Children.Add(ddStatus);
                    break;
                default:
                    break;
            }

            if (txtType != TextBoxType.id)
                dataCell.Blocks.Add(new BlockUIContainer(panel));
            infoRow.Cells.Add(dataCell);

            infoRow.Cells[1].ColumnSpan = 2;
            return infoRow;
        }
        private TableRow AddFooter(TableRow infoRow)
        {
            StackPanel panel = new StackPanel();
            panel.HorizontalAlignment = HorizontalAlignment.Left;
            panel.Orientation = Orientation.Horizontal;

            CustomButton btnSubmit = new CustomButton("Submit");
            btnSubmit._button.Click += addOrUpdateEmployee;
            panel.Children.Add(btnSubmit._button);

            if (isEditMode)
            {
                CustomButton btnBack = new CustomButton("Back");
                btnBack._button.Click += backToDetails;
                btnBack._button.Tag = employee;
                panel.Children.Add(btnBack._button);
            }

            infoRow.Cells.Add(new TableCell());
            infoRow.Cells.Add(new TableCell(new BlockUIContainer(panel)));
            infoRow.Cells[1].ColumnSpan = 2;

            return infoRow;
        }
        #endregion

        #region Value Change Events
        public void EmpNameChanged(object sender, TextChangedEventArgs e)
        { employee.name = txtName.Text.Trim(); }

        public void EmpEmailChanged(object sender, TextChangedEventArgs e)
        { employee.email = txtEmail.Text.Trim(); }

        public void EmpGenderChanged(object sender, SelectionChangedEventArgs e)
        { employee.gender = ddGender.SelectedValue.ToString(); }

        public void EmpStatusChanged(object sender, SelectionChangedEventArgs e)
        { employee.status = ddStatus.SelectedValue.ToString(); }

        #endregion


        #region method to handle add/update employee
        public async void addOrUpdateEmployee(object sender, RoutedEventArgs e)
        {
            if (employee != null)
            {
                bool isName = true, isEmail = true, isValidEmail = true;
                if (string.IsNullOrEmpty(employee.name))
                    isName = false;
                if (string.IsNullOrEmpty(employee.email))
                    isEmail = false;
                else
                    isValidEmail = Validator.ValidateEmailAddress(employee.email);

                string validationMessage = string.Empty;

                if (!isName)
                    validationMessage += "Name is required\n";
                if (!isEmail)
                    validationMessage += "Email is required\n";
                else
                    if (!isValidEmail) validationMessage += "Invalid email is required\n";

                if (!string.IsNullOrEmpty(validationMessage))
                    MessageBox.Show(validationMessage, "Validation Failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                else
                {
                    if (!isEditMode)
                    {
                        ServerResponse response = await EmployeeHandler.Post(employee);
                        if (response != null && response.success)
                        {
                            employee = new Employee();
                            txtEmail.Text = txtName.Text = string.Empty;
                            ddStatus.SelectedIndex = ddGender.SelectedIndex = 0;
                            MessageBox.Show("New employee saved successfully.", "Saved", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else MessageBox.Show("Email address already taken.", "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        ServerResponse response = await EmployeeHandler.Put(employee);
                        if (response != null && response.success)
                        {
                            MessageBox.Show("Employee details saved successfully.", "Saved", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else MessageBox.Show("Email address already taken.", "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        #endregion


        public void backToDetails(object sender, RoutedEventArgs e)
        {
            var employee = (Employee)((Button)sender).Tag;
            if (employee != null)
            {
                Details details = new Details();
                details.employee = employee;

                details.Show();
                this.Close();
            }
        }
        public void backToList(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }

    public enum TextBoxType { id, email, name, gender, status };
    public enum Genders { Male, Female };
    public enum Status { Active, Inactive };
}



