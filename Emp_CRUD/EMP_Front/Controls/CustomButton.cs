﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace EMP_Front.Controls
{
    public class CustomButton
    {
        public Button _button { get; set; }

        private Thickness Padding = new Thickness(15, 8, 15, 8);
        private Thickness Margin = new Thickness(0, 0, 10, 0);

        public CustomButton(string content, ButtonType buttonType = ButtonType.Default)
        {
            this._button = new Button();
            this._button.Content = content;
            this._button.FontSize = 14;

            if (buttonType == ButtonType.Link)
            {
                var brushConverter = new BrushConverter();
                this._button.Background = (Brush)brushConverter.ConvertFrom("#FFF");
                this._button.Foreground = (Brush)brushConverter.ConvertFrom("#0000FF");
                this._button.BorderThickness = new Thickness(0);

                this._button.Padding = new Thickness(0);
                this._button.Margin = new Thickness(5, 0, 5, 0);
            }
            else
            {
                this._button.Padding = Padding;
                this._button.Margin = Margin;
            }
        }
    }

    public enum ButtonType { Link, Danger, Success, Default }

}
