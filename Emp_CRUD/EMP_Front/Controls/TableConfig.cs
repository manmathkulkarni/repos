﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace EMP_Front.Controls
{
    public static class TableConfig
    {
        private static TableRow loader;

        public static TableRowGroup ShowLoader(TableRowGroup dataRowGroup, int columns)
        {
            loader = new TableRow();
            TableCell loaderContent = new TableCell(new Paragraph(new Run("fetching data...")));
            loader.Cells.Add(loaderContent);
            loader.Cells[0].ColumnSpan = columns;

            dataRowGroup.Rows.Add(loader);
            return dataRowGroup;
        }

        public static TableRowGroup HideLoader(TableRowGroup dataRowGroup)
        {
            dataRowGroup.Rows.Remove(loader);
            return dataRowGroup;
        }
    }
}
