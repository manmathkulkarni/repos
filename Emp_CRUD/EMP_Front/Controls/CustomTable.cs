﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace EMP_Front.Controls
{
    public class CustomTable
    {
        public Table _table1 { get; set; }
        private int columns { get; set; }
        private int currentRowNum { get; set; }
        private int currentRowGroupNum { get; set; }

        private TableRow loader { get; set; }
        private TableRow status { get; set; }

        public TextBox txtSearchBox { get; set; }
        public CustomButton searchBtn { get; set; }


        public CustomTable() { }

        public CustomTable(int columns)
        {
            this._table1 = new Table();
            this._table1.Padding = new Thickness(10);
            this._table1.CellSpacing = 10;
            this._table1.Background = Brushes.White;

            this.currentRowNum = 0;
            this.currentRowGroupNum = 0;

            this.columns = columns;
            for (var x = 0; x < columns; x++) this._table1.Columns.Add(new TableColumn());

            this.loader = new TableRow();
        }

        public void AddTitle(string title, CustomButton addEmployeeBtn)
        {
            this._table1.RowGroups.Add(new TableRowGroup());
            var rowTitle = new TableRow();

            rowTitle.FontSize = 24;
            rowTitle.FontWeight = FontWeights.Bold;

            rowTitle.Cells.Add(new TableCell(new Paragraph(new Run(title))));
            rowTitle.Cells[0].ColumnSpan = columns - 2;


            if (addEmployeeBtn != null)
            {
                StackPanel btnPanel = new StackPanel();
                btnPanel.HorizontalAlignment = HorizontalAlignment.Right;
                btnPanel.Orientation = Orientation.Horizontal;
                btnPanel.Children.Add(addEmployeeBtn._button);

                rowTitle.Cells.Add(new TableCell(new BlockUIContainer(btnPanel)));
                rowTitle.Cells[1].ColumnSpan = 2;
            }

            this._table1.RowGroups[currentRowGroupNum].Rows.Add(rowTitle);
            this.currentRowGroupNum = this.currentRowGroupNum + 1;
        }

        public void AddHeader(string[] headers, bool isSearchBoxAdded)
        {
            this._table1.RowGroups.Add(new TableRowGroup());

            if (isSearchBoxAdded)
            {
                var searchRow = new TableRow();
                searchRow.FontSize = 16;
                searchRow.Cells.Add(new TableCell());

                StackPanel searchPanel = new StackPanel();

                txtSearchBox = new TextBox();
                txtSearchBox.Padding = new Thickness(5);
                searchPanel.Children.Add(txtSearchBox);
                searchRow.Cells.Add(new TableCell(new BlockUIContainer(searchPanel)));

                searchPanel = new StackPanel();
                searchBtn = new CustomButton("Search");
                searchPanel.Children.Add(searchBtn._button);
                searchBtn._button.Padding = new Thickness(10, 5, 10, 5);

                searchRow.Cells.Add(new TableCell(new BlockUIContainer(searchPanel)));
                searchRow.Cells[1].ColumnSpan = 2;

                this._table1.RowGroups[currentRowGroupNum].Rows.Add(searchRow);
                this.currentRowNum = this.currentRowNum + 1;
            }

            var header = new TableRow();
            header.FontSize = 16;
            header.FontWeight = FontWeights.Bold;
            this._table1.RowGroups[currentRowGroupNum].Rows.Add(header);

            var currentRow = this._table1.RowGroups[currentRowGroupNum].Rows[currentRowNum];
            this.currentRowNum = this.currentRowNum + 1;

            var brushConverter = new BrushConverter();
            currentRow.Background = (Brush)brushConverter.ConvertFrom("#FFe2e2e2");

            // Add cells with content to the second row.
            for (int i = 0; i < headers.Length; i++)
            {
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(headers[i]))));
            }
        }

        public void ShowStatus(string statusText)
        {
            status = new TableRow();
            status.FontSize = 16; status.Foreground = Brushes.Blue; status.FontWeight = FontWeights.Bold;

            status.Cells.Add(new TableCell(new Paragraph(new Run(statusText))));
            status.Cells[0].ColumnSpan = columns;
            this._table1.RowGroups[0].Rows.Add(status);
        }

        public void RemoveStatus()
        {
            this._table1.RowGroups[0].Rows.Remove(status);
        }


        public void CreateRowGroupForData()
        {
            this._table1.RowGroups.Add(new TableRowGroup());
            this.currentRowGroupNum = this.currentRowGroupNum + 1;
            this.currentRowNum = 0;
        }

        public void AddRow(string[] data, List<CustomButton> btns)
        {
            this._table1.RowGroups[currentRowGroupNum].Rows.Add(new TableRow());
            var currentRow = this._table1.RowGroups[currentRowGroupNum].Rows[currentRowNum]; currentRowNum = currentRowNum + 1;

            // Global formatting for the header row.
            currentRow.FontSize = 14;

            // Add cells with content to the second row.
            for (int i = 0; i < data.Length; i++)
            {
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run(data[i]))));
            }

            StackPanel btnPanel = new StackPanel();
            btnPanel.HorizontalAlignment = HorizontalAlignment.Right;
            btnPanel.Orientation = Orientation.Horizontal;

            foreach (var btn in btns)
            {
                btnPanel.Children.Add(btn._button);
            }
            currentRow.Cells.Add(new TableCell(new BlockUIContainer(btnPanel)));
        }

        public void AddFooter(int pageNum, int maxPage, int pageLimit, int total, List<CustomButton> allCustomBtns)
        {
            this._table1.RowGroups[currentRowGroupNum].Rows.Add(new TableRow());
            var currentRow = this._table1.RowGroups[currentRowGroupNum].Rows[currentRowNum]; currentRowNum = currentRowNum + 1;
            currentRow.FontSize = 14;
            currentRow.FontWeight = FontWeights.Bold;

            Paragraph paragraph = new Paragraph(new Run("Page No: " + pageNum + "/" + maxPage + "\nPage Limit:  " + pageLimit + " records \nTotal Records: " + total));
            currentRow.Cells.Add(new TableCell(paragraph));
            currentRow.Cells[0].ColumnSpan = columns - 3;

            StackPanel btnPanel = new StackPanel();
            btnPanel.HorizontalAlignment = HorizontalAlignment.Right;
            btnPanel.Orientation = Orientation.Horizontal;

            foreach (var btn in allCustomBtns)
            {
                btnPanel.Children.Add(btn._button);
            }
            currentRow.Cells.Add(new TableCell(new BlockUIContainer(btnPanel)));
            currentRow.Cells[1].ColumnSpan = 3;

        }

        public void NoRecordFound()
        {
            this._table1.RowGroups[currentRowGroupNum].Rows.Add(new TableRow());
            this._table1.RowGroups[currentRowGroupNum].Rows[currentRowNum].Cells.Add(new TableCell(new Paragraph(new Run("No Record Found"))));
            this._table1.RowGroups[currentRowGroupNum].Rows[currentRowNum].Cells[0].ColumnSpan = columns;

            currentRowNum = currentRowNum + 1;
        }

        public void RemoveAllData()
        {
            this._table1.RowGroups.RemoveAt(currentRowGroupNum);
            this.currentRowGroupNum = this.currentRowGroupNum - 1;
            this.currentRowNum = 1;
        }

        public void ShowLoader()
        {
            loader = new TableRow();
            TableCell loaderContent = new TableCell(new Paragraph(new Run("fetching data...")));
            loader.Cells.Add(loaderContent);
            loader.Cells[0].ColumnSpan = columns;

            this._table1.RowGroups[currentRowGroupNum].Rows.Add(loader);
        }
        public void HideLoader()
        {
            this._table1.RowGroups[currentRowGroupNum - 1].Rows.Remove(loader);
        }
    }

}
