﻿using EMP_Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMP_Front.ViewModel
{
    public class EmployeeViewModel : ViewModelBase
    {
        #region fields
        // wrapped Person object
        private readonly Employee _employee;
        #endregion


        /// <summary>
        /// Create a new instance of the PersonViewModel class
        /// </summary>
        /// <param name="person">Wrapped person object</param>
        public EmployeeViewModel(Employee employee)
        {
            if (employee == null)
                throw new NullReferenceException("person");

            this._employee = employee;
        }


        #region properties
        public string name
        {
            get
            {
                return this._employee.name;
            }
            set
            {
                this._employee.name = value;
                OnPropertyChanged("name");
            }
        }

        public string email
        {
            get
            {
                return this._employee.email;
            }
            set
            {
                this._employee.email = value;
                OnPropertyChanged("email");
            }
        }

        public string status
        {
            get
            {
                return this._employee.status;
            }
            set
            {
                this._employee.status = value;
                OnPropertyChanged("status");
            }
        }


        public string gender
        {
            get
            {
                return this._employee.gender;
            }
            set
            {
                this._employee.gender = value;
                OnPropertyChanged("gender");
            }
        }

        public string created_at
        {
            get
            {
                return this._employee.created_at;
            }
            set
            {
                this._employee.created_at = value;
                OnPropertyChanged("created_at");
            }
        }

        public string updated_at
        {
            get
            {
                return this._employee.updated_at;
            }
            set
            {
                this._employee.updated_at = value;
                OnPropertyChanged("updated_at");
            }
        }




        public Employee Person
        {
            get
            {
                return this._employee;
            }
        }
        #endregion
    }
}
