﻿using EMP_Data.Handler;
using EMP_Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emp_CRUD.Access
{
    public class EmployeeAccessor
    {
        public async Task<List<Employee>> All(Employee employee, int pageNum)
        {
            List<Employee> employees = new List<Employee>();
            ServerResponse response = await EmployeeHandler.Get(employee, pageNum);

            if (response.success)
                employees = response.data;

            return employees;
        }


        public async Task<List<Employee>> Remove(int pageNum)
        {
            List<Employee> employees = new List<Employee>();

            Employee employee = new Employee();
            ServerResponse response = await EmployeeHandler.Get(employee, pageNum);

            if (response.success)
                employees = response.data;

            return employees;
        }
    }
}
