﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using EMP_Data.Handler;
using EMP_Data.Models;
using System;
using System.Linq;

namespace EMP_Data.test.Handler
{
    [TestClass]
    public class EmployeeHandlerTest
    {
        string random = DateTime.UtcNow.ToString("HHmmss");

        [TestMethod]
        public async Task Get_ValidCall()
        {
            // Arrange
            int pageNum = 1;

            // Act
            var output = await EmployeeHandler.Get(null, pageNum);

            // Assert
            Assert.IsNotNull(output);
            Assert.AreEqual(output.success, true);
            Assert.IsNotNull(output.data);
        }

        [TestMethod]
        public async Task Get_InvalidCall()
        {
            // Arrange
            int pageNum = -1;

            // Act
            var output = await EmployeeHandler.Get(null, pageNum);

            // Assert
            Assert.IsNotNull(output);
            Assert.AreEqual(output.success, false);
            Assert.IsNull(output.data);
        }

        [TestMethod]
        public async Task Post_WithInvalidData_ReturnsUnsuccessfulResult()
        {
            // Arrange
            // Act
            var output = await EmployeeHandler.Post(new Employee());

            // Assert
            Assert.IsNotNull(output);
            Assert.IsFalse(output.success);
            Assert.IsTrue(output.code >= 400);
        }

        [TestMethod]
        public async Task Post_WithValidData_ReturnsSuccessfulResult()
        {
            // Arrange
            Employee emp = new Employee()
            {
                name = "testCase" + random,
                email = "testCase" + random + "@domain.com",
                gender = "Male",
                status = "Inactive"
            };
            // Act
            var output = await EmployeeHandler.Post(emp);

            // Assert
            Assert.IsNotNull(output);
            Assert.IsTrue(output.success);
            Assert.IsTrue(output.code < 300 && output.code >= 200);
        }

        [TestMethod]
        public async Task Put_WithInvalidData_ReturnsUnsuccessfulResult()
        {
            // Arrange

            // Act
            var output = await EmployeeHandler.Put(new Employee());

            // Assert
            Assert.IsNotNull(output);
            Assert.IsFalse(output.success);
            Assert.IsTrue(output.code >= 400);
        }

        [TestMethod]
        public async Task Put_WithValidData_ReturnsUnsuccessfulResult()
        {
            // Arrange
            Employee emp = await GetFirstRecordFromList();
            if (emp == null) return;

            emp.name = "updated" + random;
            emp.email = "updated" + random + "@domain.com";
            emp.gender = "Female";
            emp.status = "Active";

            // Act
            var output = await EmployeeHandler.Put(emp);

            // Assert
            Assert.IsNotNull(output);
            Assert.IsTrue(output.success);
            Assert.IsTrue(output.code < 300 && output.code >= 200);
        }


        [TestMethod]
        public async Task Delete_WithInvalidId_ReturnsNotFound()
        {
            // Arrange
            Employee emp = new Employee() { id = 0 };
            // Act
            var output = await EmployeeHandler.Delete(emp);

            // Assert
            Assert.IsNotNull(output);
            Assert.IsFalse(output.success);
            Assert.IsTrue(output.code >= 400);
        }

        [TestMethod]
        public async Task Delete_WithValidId_ReturnsOk()
        {
            // Arrange
            Employee emp = await GetFirstRecordFromList();
            if (emp == null) return;

            // Act
            var output = await EmployeeHandler.Delete(emp);

            // Assert
            Assert.IsNotNull(output);
            Assert.IsTrue(output.success);
            Assert.IsTrue(output.code < 300 && output.code >= 200);
        }


        [TestMethod]
        public async Task GetById_WithInvalidId_ReturnsNotFound()
        {
            // Arrange
            Employee emp = new Employee() { id = 0 };
            // Act
            var output = await EmployeeHandler.GetById(emp);

            // Assert
            Assert.IsNotNull(output);
            Assert.IsFalse(output.success);
            Assert.IsTrue(output.code >= 400);
            Assert.IsTrue(output.data.id == 0);
        }

        [TestMethod]
        public async Task GetById_WithValidId_ReturnsEmployee()
        {
            // Arrange
            Employee emp = await GetFirstRecordFromList();
            if (emp == null) return;

            // Act
            var output = await EmployeeHandler.GetById(emp);

            // Assert
            Assert.IsNotNull(output);
            Assert.IsTrue(output.success);
            Assert.IsTrue(output.code < 300 && output.code >= 200);
            Assert.IsNotNull(output.data);
            Assert.IsTrue(output.data.id > 0);
        }




        private async Task<Employee> GetFirstRecordFromList()
        {
            Employee emp = new Employee();
            ServerResponse response = await EmployeeHandler.Get(new Employee(), 1);
            if (response.code >= 200 && response.code < 300 && response.data != null && response.data.Count > 0)
                emp = response.data.First();
            else emp = null;

            return emp;
        }
    }
}
