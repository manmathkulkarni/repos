﻿using EMP_Data.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMP_Data.test.Utility
{
    [TestClass]
    public class ValidatorTest
    {
        [TestMethod]
        public void ValidateEmailAddress_WithInvalidEmail_ReturnsFalse()
        {
            var response = Validator.ValidateEmailAddress("emailaddress");
            Assert.IsFalse(response);
        }

        [TestMethod]
        public void ValidateEmailAddress_WithValidEmail_ReturnsTrue()
        {
            var response = Validator.ValidateEmailAddress("vaild@email.com");
            Assert.IsTrue(response);
        }
    }
}
