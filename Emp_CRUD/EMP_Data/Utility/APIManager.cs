﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EMP_Data.Utility
{

    public static class ApiValues
    {
        public static string RootUrl = "https://gorest.co.in/public-api/";
        public static string Key = "fa114107311259f5f33e70a5d85de34a2499b4401da069af0b1d835cd5ec0d56";
        
        public static string url = "users";
    }

    public static class ApiManager<T>
    {
        public static string rootApiUrl = ApiValues.RootUrl;
        public static string authToken = ApiValues.Key;

        public static async Task<string> GetAsync(string baseUrl, T model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrEmpty(authToken))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);

                HttpResponseMessage responseMessage = await client.GetAsync(baseUrl);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync();
                    return responseData.Result;
                }
                return string.Empty;
            }
        }

        public static async Task<string> PostAsync(string baseUrl, string model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrEmpty(authToken))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);

                var content = new StringContent(model.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage responseMessage = await client.PostAsync(baseUrl, content);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync();
                    return responseData.Result;
                }
                return string.Empty;
            }
        }

        public static async Task<string> PutAsync(string baseUrl, string model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrEmpty(authToken))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);

                var content = new StringContent(model.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage responseMessage = await client.PutAsync(baseUrl, content);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync();
                    return responseData.Result;
                }
                return string.Empty;
            }
        }

        public static async Task<string> DeleteAsync(string baseUrl, T model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(rootApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrEmpty(authToken))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);

                HttpResponseMessage responseMessage = await client.DeleteAsync(baseUrl);
                if (responseMessage.IsSuccessStatusCode)
                {
                    var responseData = responseMessage.Content.ReadAsStringAsync();
                    return responseData.Result;
                }
                return string.Empty;
            }
        }

     

    }

}
