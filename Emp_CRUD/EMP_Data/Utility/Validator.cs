﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace EMP_Data.Utility
{
    public static class Validator
    {
        public static bool ValidateEmailAddress(string emailaddress)
        {
            try
            {
                if (string.IsNullOrEmpty(emailaddress)) return false;
                MailAddress mail = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException) { return false; }
        }
    }
}
