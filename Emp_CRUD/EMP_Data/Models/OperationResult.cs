﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMP_Data.Models
{
    public class OperationResult<T> where T : class
    {
        public OperationResult() { }

        public OperationResult(bool success)
        {
            this.Success = success;
        }
        public OperationResult(bool success, string message)
        {
            this.Success = success; this.Message = message;
        }
        public OperationResult(bool success, string message, T state)
        {
            this.Success = success; this.Message = message; this.State = state;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public T State { get; set; }
    }
}
