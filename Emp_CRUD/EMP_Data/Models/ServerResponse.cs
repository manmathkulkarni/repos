﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace EMP_Data.Models
{
    [DataContract]
    public class ServerResponse
    {
        [DataMember]
        public int code { get; set; }
        [DataMember]
        public meta meta { get; set; }

        [DataMember]
        public List<Employee> data { get; set; }

        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public string message { get; set; }

        public ServerResponse()
        {
            this.success = false;
        }

        public ServerResponse(bool success, string message)
        {
            this.success = success;
            this.message = message;
        }
    }

    public class meta
    {
        public pagination pagination { get; set; }
    }

    public class pagination
    {
        public int total { get; set; }
        public int pages { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
    }


    [DataContract]
    public class SingleResponse
    {
        [DataMember]
        public int code { get; set; }
        
        [DataMember]
        public meta meta { get; set; }

        [DataMember]
        public Employee data { get; set; }

        [DataMember]
        public bool success { get; set; }

        [DataMember]
        public string message { get; set; }

        public SingleResponse()
        {
            this.success = false;
        }

        public SingleResponse(bool success, string message)
        {
            this.success = success;
            this.message = message;
        }
    }


}

