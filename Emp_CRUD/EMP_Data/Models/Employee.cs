﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMP_Data.Models
{
    public class Employee
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }

    }
}
