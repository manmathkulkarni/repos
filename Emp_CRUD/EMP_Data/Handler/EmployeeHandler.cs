﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

using EMP_Data.Models;
using EMP_Data.Utility;

namespace EMP_Data.Handler
{
    public static class EmployeeHandler
    {
        public static async Task<ServerResponse> Get(Employee employee, int pageNum)
        {
            var url = ApiValues.url + "?page=" + pageNum;
            if (employee != null && !string.IsNullOrEmpty(employee.name))
                url += "&name=" + employee.name;

            var result = await ApiManager<Employee>.GetAsync(url, employee);
            var output = new ServerResponse();

            if (!string.IsNullOrEmpty(result))
            {
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(result)))
                {
                    // Deserialization from JSON  
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(ServerResponse));
                    output = (ServerResponse)deserializer.ReadObject(ms);
                }
                if (output.code >= 200 && output.code < 300) output.success = true;
            }
            else
            {

                output = new ServerResponse(false, "Internal server error occured");
                return output;
            }

            return output;
        }

        public static async Task<ServerResponse> Post(Employee employee)
        {
            var empData = "{\"email\": \"" + employee.email
                            + "\",\"name\": \"" + employee.name
                            + "\",\"gender\": \"" + employee.gender
                            + "\",\"status\": \"" + employee.status + "\"}";

            var result = await ApiManager<Employee>.PostAsync(ApiValues.url, empData);
            var output = new ServerResponse();

            if (!string.IsNullOrEmpty(result))
            {
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(result)))
                {
                    // Deserialization from JSON  
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(ServerResponse));
                    output = (ServerResponse)deserializer.ReadObject(ms);
                }

                if (output.code >= 200 && output.code < 300) output.success = true;
            }
            else
            {
                output = new ServerResponse(false, "Internal server error occured");
                return output;
            }

            return output;
        }

        public static async Task<ServerResponse> Put(Employee employee)
        {
            var url = ApiValues.url + "/" + employee.id;

            var empData = "{\"email\": \"" + employee.email
                          + "\",\"name\": \"" + employee.name
                          + "\",\"gender\": \"" + employee.gender
                          + "\",\"status\": \"" + employee.status + "\"}";


            var result = await ApiManager<Employee>.PutAsync(url, empData);
            var output = new ServerResponse();

            if (!string.IsNullOrEmpty(result))
            {
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(result)))
                {
                    // Deserialization from JSON  
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(ServerResponse));
                    output = (ServerResponse)deserializer.ReadObject(ms);
                }

                if (output.code >= 200 && output.code < 300) output.success = true;
            }
            else
            {
                output = new ServerResponse(false, "Internal server error occured");
                return output;
            }

            return output;
        }

        public static async Task<ServerResponse> Delete(Employee employee)
        {
            var url = ApiValues.url + "/" + employee.id;
            var result = await ApiManager<Employee>.DeleteAsync(url, employee);
            var output = new ServerResponse();

            if (!string.IsNullOrEmpty(result))
            {
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(result)))
                {
                    // Deserialization from JSON  
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(ServerResponse));
                    output = (ServerResponse)deserializer.ReadObject(ms);
                }

                if (output.code >= 200 && output.code < 300) output.success = true;
            }
            else
            {
                output = new ServerResponse(false, "Internal server error occured");
                return output;
            }

            return output;
        }

        public static async Task<SingleResponse> GetById(Employee employee)
        {
            var url = ApiValues.url + "/"+ employee.id;
            var result = await ApiManager<Employee>.GetAsync(url, employee);
            var output = new SingleResponse();

            if (!string.IsNullOrEmpty(result))
            {
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(result)))
                {
                    // Deserialization from JSON  
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(SingleResponse));
                    output = (SingleResponse)deserializer.ReadObject(ms);
                }
                if (output.code >= 200 && output.code < 300) output.success = true;
            }
            else
            {
                output = new SingleResponse(false, "Internal server error occured");
                return output;
            }

            return output;
        }
    }
}
